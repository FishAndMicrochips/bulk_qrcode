#!/usr/bin/env python
# This is a little script which takes in a list of files containing links,
# and outputs QR codes which, when scanned, point to each link.
# The files are simply lists of URLs. For example, a file might simply contain:
# https://www.google.com
# marxists.org
# en.wikipedia.org
# NOTE: Remove the '#' in the files.
import os
import sys
import qrcode
import re

files = sys.argv[1:]
image_file_extension = 'png'

def remove_url_stuff(link_str):
    without_www_dot_something = lambda my_str: re.sub(r'(^[a-zA-Z]+://)?(www\.)?', '', my_str)
    without_domains           = lambda my_str: re.sub(r'\.(org|com|net|co\.uk)', '', my_str)
    without_fwd_slashes       = lambda my_str: my_str.replace('/', '-')
    without_file_extension    = lambda my_str: re.sub('\.[a-zA-Z]+$', '', my_str)
    stripped_url_stuff =\
        without_file_extension(
            without_fwd_slashes(
                without_domains(
                    without_www_dot_something(link_str))))
    if stripped_url_stuff[-1] == '-':
        stripped_url_stuff = stripped_url_stuff[:-1]
    return stripped_url_stuff

for file_name in files:
    with open(file_name, 'r') as fd:
        for link in fd:
            link = link.strip()
            png_name = f'{remove_url_stuff(link)}.{image_file_extension}'
            print(f'{link} -> {png_name}')
            img = qrcode.make(link,
                              version=None,
                              error_correction=qrcode.constants.ERROR_CORRECT_Q,
                              box_size=100,
                              border=10)
            img.save(png_name)
