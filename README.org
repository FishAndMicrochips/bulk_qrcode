#+TITLE: ~bulk_qrcode~
#+AUTHOR: Amber Gingell
* What is this?
  This is a little script which takes in a list of files containing links,
  and outputs QR codes which, when scanned, point to each link.
* How do you install it?
** 1. Cloning the Repository
   To clone the repository, one uses ~git clone~ on the URL:
   #+begin_src
     git clone https://gitlab.com/FishAndMicrochips/bulk_qrcode.git
     cd bulk_qrcode
   #+end_src
** 2. Installing the dependencies
   I am assuming you have ~pip~ installed.
   All the dependencies are in ~requirements.txt~.
  To install the packages, you use the following command:
  #+begin_src
     pip install -r requirements.txt
  #+end_src
  If your package manager uses something other than ~pip~,
  I recommend you search for ~qrcode~ on your package manager of choice.
  For example, on arch-based distributions, the ~pacman~ package is called ~python-qrcode~
  at time of writing.
** 3. Making the script executable (MacOS or Linux)
   On Linux and MacOS, you use the ~chmod~ command:
  #+begin_src
     chmod +x bulk_qrcode.py
  #+end_src
* How do you use it?
  On MacOS or Linux You use the utility like this:
  #+begin_src
    bulk_qrcode.py file1 file2 file3
  #+end_src
    On Windows, you need to run the command like this:
  #+begin_src
    python bulk_qrcode.py file1 file2 file3
  #+end_src
  The files are simply lists of URLs. For example, a file might simply contain:
  #+begin_src
    https://www.google.com
    xkcd.com
    en.wikipedia.org
  #+end_src
